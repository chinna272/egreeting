import React, { useState } from 'react';
import { View, ToastAndroid } from 'react-native';
import { RNCamera} from 'react-native-camera'
import { useCamera } from 'react-native-camera-hooks';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from '../InputScreen/styles';

const CameraScreen = (props) => {

    const { navigation, route } = props;

    const { saveUserProfileImage } = route?.params ?? {};
    const [{cameraRef}, {takePicture}] = useCamera(null);
    const [isFrontCameEnable, setIsFrontCameEnable] = useState(false)

    const onCaptureImage = async() => {
        try{
            const imageData = await takePicture();
            const imagePath = imageData.uri;
            ToastAndroid.show('Successfully photo taken', ToastAndroid.BOTTOM);
            saveUserProfileImage(imagePath);
            navigation.goBack();
        }catch(error){
            console.log(error);
        }
    }
    return <View style={{flex:1}}>
        <RNCamera
            style={styles.cameraContainer}
            ref={cameraRef}
            type={isFrontCameEnable ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back}
        >
            <View style={styles.cameraIconContainer}>
                <Icon onPress={onCaptureImage} name='camera' size={40} color={'#fff'} />
                <Icon onPress={() =>  setIsFrontCameEnable(!isFrontCameEnable)} name='camera-reverse-sharp' size={40} color={'#fff'} />
            </View>
        </RNCamera>
    </View>
};

export default CameraScreen;