import { createStore, combineReducers } from 'redux';

import useDataReducer from '../reducer/UserDetailsReducer';


const rootReducer = combineReducers(
    { userDetails:  useDataReducer}
);

const store = () => {
    return createStore(rootReducer);
};

export default store;