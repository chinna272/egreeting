import React, { useState } from 'react';
import { connect } from 'react-redux';

import { View, Text, TextInput, TouchableOpacity, ToastAndroid } from 'react-native';
import { storeUserDetails } from '../action/UserDetailsAction';

import styles from './styles';

const InputScreen = (props) => {

  const { navigation, storeUserDetails } = props;
  
  const [useDetails, setUseDetails] = useState({name: '',greeting: '', image: ''});
  const isSaveButtonEnable = useDetails?.name && useDetails?.greeting && useDetails?.image;

  const onChangeName = (useName) => {
    setUseDetails({...useDetails, name: useName});
  }
  const onChangeGreeting = (greeting) => {
    setUseDetails({...useDetails, greeting: greeting});
  }

  const saveUserProfileImage = (url) => {
    setUseDetails({...useDetails, image: url});
  };
  const saveUserData = () => {
    storeUserDetails(useDetails);
    navigation.goBack();
    ToastAndroid.show('Successfully data saved', ToastAndroid.BOTTOM);
  };
    return (
      <View style={styles.container}>
        <TextInput
          onChangeText={onChangeName}
          placeholder={'Please enter your name!'}
          style={styles.inputStyle}
        />
        <TextInput
          onChangeText={onChangeGreeting}
          placeholder={'Please enter the greeting you like!'}
          style={styles.inputStyle}
        />
        <TouchableOpacity onPress={() => navigation.navigate('CameraScreen', {saveUserProfileImage})} style={styles.photoButton}>
          <Text>Take a photo</Text>
        </TouchableOpacity>
        <TouchableOpacity disabled={!isSaveButtonEnable} onPress={saveUserData} style={[styles.photoButton, !isSaveButtonEnable ? styles.inActive : {}]}>
          <Text>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
  export default connect(null, {storeUserDetails})(InputScreen);