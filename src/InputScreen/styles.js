import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: '#34495E'
    },
    inputStyle: {
        fontSize: 16,
        color: '#ffffff',
        borderWidth: 1,
        borderRadius: 8,
        padding: 6,
        marginBottom: 16
    },
    photoButton: {
        backgroundColor: 'orange',
        width: 100,
        height: 40,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 6,
    },
    inActive: {
        backgroundColor: '#85929E'
    },
    cameraContainer: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    cameraIconContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
})