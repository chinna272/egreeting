export const STORE_USER_DETAILS = 'STORE_USER_DETAILS';

export function storeUserDetails(userData) {
    return {
        type: STORE_USER_DETAILS,
        payload: userData,
    };
};