import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    bgImage: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 16,
        color: '#ffffff',
        textAlign: 'center',
        paddingVertical: 6,
    },
    textContainer: {
        backgroundColor: '#000000',
        borderRadius: 8,
        marginHorizontal: 16,
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyScreen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})