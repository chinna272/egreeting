import React from 'react';
import { connect } from 'react-redux';

import { View, Text, ImageBackground } from 'react-native';

import styles from './styles';
const defaultImage = "https://reactjs.org/logo-og.png";

const DisplayScreen = (props) => {

  const { useDetails } = props || {};

  const { greeting = '', image = '', name  = ''} = useDetails;
  const notEmpty = greeting && image && name;

  if(notEmpty) {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={{uri: image || defaultImage}}
          resizeMode={'cover'}
          style={styles.bgImage}
        >
          <View style={styles.textContainer}>
            <Text style={styles.textStyle}>{name || ''}</Text>
            <Text style={styles.textStyle}>{greeting || ''}</Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
  return (
    <View style={styles.emptyScreen}>
      <Text style={{color: '#000000', fontSize: 20, textAlign: 'center'}}>Empty data!, please enter some data from input screen</Text>
    </View>
  );

};

const mapstateToProps = state => {
  return {
    useDetails: state?.userDetails?.userData ?? {},
  };
};

export default connect(mapstateToProps, null)(DisplayScreen);