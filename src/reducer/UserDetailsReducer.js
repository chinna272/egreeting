import { STORE_USER_DETAILS } from '../action/UserDetailsAction';

const initialState = {
    userData: {},
};

const userDetailsReducer = (state = initialState, action) => {
    switch(action.type) {
        case STORE_USER_DETAILS:
            return {
                ...state,
                userData: action.payload,
            };
        default:
            return state;
    }
};

export default userDetailsReducer;