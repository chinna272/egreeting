import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './HomeScreen';
import InputScreen from './InputScreen';
import DisplayScreen from './DisplayScreen';
import CameraScreen from './CameraScreen';


const Stack = createNativeStackNavigator();

const Route = () => {
    return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen 
                options={{
                    title: 'EGreeting',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'green'
                    }
                }}
                name="HomeScreen" 
                component={HomeScreen} 
            />
            <Stack.Screen
                options={{
                    title: 'Input screen',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'blue'
                    }
                }}
                name="InputScreen" 
                component={InputScreen} 
             />
            <Stack.Screen
                options={{
                    title: 'Display screen',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'blue'
                    }
                }}
                name="DisplayScreen" 
                component={DisplayScreen} 
             />
             <Stack.Screen
                options={{
                    title: 'Camera screen',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'orange'
                    }
                }}
                name="CameraScreen" 
                component={CameraScreen} 
             />
          </Stack.Navigator>
        </NavigationContainer>
      ) 
};

export default Route;