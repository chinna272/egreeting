import React from 'react';

import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

const HomeScreen = (props) => {

    const { navigation } = props;

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.screenCard} onPress={() => navigation.navigate('InputScreen')}>
                <Text style={styles.screenText}>Input Screen</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.screenCard} onPress={() => navigation.navigate('DisplayScreen')}>
                <Text style={styles.screenText}>Display Screen</Text>
             </TouchableOpacity>
        </View>
    );
};

export default HomeScreen;