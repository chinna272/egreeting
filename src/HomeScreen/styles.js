import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'space-around',
        backgroundColor: '#ffffff',
    },
    screenText: {
        fontSize: 24,
        color: '#000000',
    },
    screenCard: {
        borderColor: '#808B96',
        borderRadius: 1.5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 16,

        shadowColor: '#808B96',
        shadowOffset: {
            width: 1,
            height: 0
        },
        shadowOpacity: 0.2,
        shadowRadius: 0.15,
        elevation: 0.5,
    },
})